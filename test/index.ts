import * as tape from "tape";
import {createQueue, Consumer, Producer} from "../src";
import {spy} from "sinon";
import {ConsumerRunConfig} from "kafkajs";

const createConsumer = (
    run: (eachMessage: ConsumerRunConfig["eachMessage"]) => void
): Consumer => {
    return {
        disconnect(): Promise<void> {
            return Promise.resolve(undefined);
        },
        connect() {
            return Promise.resolve();
        },
        run(config: ConsumerRunConfig) {
            console.log('RUN', config);

            run(config.eachMessage);

            return Promise.resolve();
        },
        subscribe() {
            return Promise.resolve();
        }
    };
};

const createProducer = (): Producer => {
    return {
        connect() {
            return Promise.resolve();
        },
        send() {
            return Promise.resolve([]);
        },
        disconnect() {
            return Promise.resolve(undefined);
        }
    }
};

tape('createQueue', ({test}) => {
    test('creates as many consumer and producers as the number of retries plus one', ({same, end}) => {
        const spiedCreateConsumer = spy(createConsumer);

        const candidate = createQueue(
            'foo',
            3,
            {
                consumer: () => spiedCreateConsumer(() => {
                }),
                producer: () => createProducer()
            },
            {
                add: () => Promise.resolve(),
                has: () => Promise.resolve(true)
            }
        );

        return candidate(
            () => Promise.resolve(),
            () => Promise.resolve()
        ).then(() => {
            same(spiedCreateConsumer.callCount, 4);

            end();
        });
    });
});