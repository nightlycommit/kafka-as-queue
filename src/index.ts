import type {Message, Consumer as KafkaConsumer, Producer as KafkaProducer} from "kafkajs";
import {v4} from "uuid";

export type StartedQueue<Item> = {
    push: (item: Item) => Promise<void>;
    stop: () => Promise<void>;
};

export type Queue<Item> = (
    executor: (item: Item) => Promise<void>,
    callback: (item: Item, error: Error | null) => Promise<void>
) => Promise<StartedQueue<Item>>;

export type Repository = {
    has: (identifier: string) => Promise<boolean>;
    add: (identifier: string) => Promise<void>;
};

type InternalMessage = Omit<Message, "value" | "headers"> & {
    value: Buffer;
    headers: {
        identifier: Buffer;
    };
};

const isInternalMessage = (message: any): message is InternalMessage => {
    const candidate: Partial<InternalMessage> = message;

    return (candidate.value !== undefined) &&
        (candidate.headers !== undefined) &&
        (candidate.headers.identifier !== undefined);
};

export type Consumer = Pick<KafkaConsumer, "connect" | "disconnect" | "run" | "subscribe">;
export type Producer = Pick<KafkaProducer, "connect" | "disconnect" | "send">;

export const createQueue = <Item>(
    name: string,
    numberOfRetries: number,
    client: {
        consumer: (configuration: {
            groupId: string
        }) => Consumer;
        producer: () => Producer
    },
    repository: Repository
): Queue<Item> => {
    const createQueue = (index: number): Queue<Item> => {
        const topic: string = `${name}` + (index > 0 ? `_${index}` : '');

        let nextQueue: Queue<Item> | undefined;

        if (index < numberOfRetries) {
            nextQueue = createQueue(index + 1);
        }

        const consumer = client.consumer({
            groupId: topic
        });

        const producer = client.producer();

        return (executor, callback) => {
            return consumer.connect().then(() => {
                return consumer.subscribe({
                    topic,
                    fromBeginning: true
                });
            }).then(() => {
                // start the next queue if any
                return nextQueue ? nextQueue(executor, callback) : null;
            }).then((nextQueue) => {
                return consumer.run({
                    autoCommit: true,
                    eachMessage: (payload) => {
                        const consume = () => {
                            const {message} = payload;

                            if (!isInternalMessage(message)) {
                                // ignore unsupported messages
                                return Promise.resolve();
                            }

                            const {headers, value} = message;

                            const item: Item = JSON.parse(value.toString());

                            const identifier = headers.identifier.toString();

                            return repository.has(identifier).then((has) => {
                                if (has) {
                                    return Promise.resolve();
                                } else {
                                    return repository.add(identifier).then(() => {
                                        return executor(item);
                                    }).then(() => {
                                        return callback(item, null);
                                    });
                                }
                            }).catch((error) => {
                                if (nextQueue) {
                                    return nextQueue.push(item);
                                }

                                return callback(item, error);
                            });
                        }

                        return new Promise((resolve) => {
                            setTimeout(() => {
                                resolve(consume());
                            }, index * 5000);
                        });
                    }
                }).then(() => {
                    return producer.connect();
                }).then(() => {
                    return {
                        push: (moneyTransferRequest) => {
                            return producer.send({
                                topic,
                                messages: [{
                                    value: Buffer.from(JSON.stringify(moneyTransferRequest)),
                                    headers: {
                                        identifier: v4()
                                    }
                                }]
                            }).then();
                        },
                        stop: () => {
                            return Promise.all([
                                consumer.disconnect(),
                                producer.disconnect(),
                                nextQueue ? nextQueue.stop() : Promise.resolve()
                            ]).then();
                        }
                    }
                })
            });
        };
    }

    return createQueue(0);
};